/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

/**
 *
 * @author Acássio
 */
public class EscritorTxt {

    private File file;
    private FileWriter escritor;
    private PrintWriter impressor;

    public EscritorTxt(File file) throws IOException {
        this.file = file;
        file.createNewFile();

    }

    public EscritorTxt(String dir, String arq) throws IOException {
        File diretorio = new File(dir);
        file = new File(diretorio, arq);
        file.delete();
        file.createNewFile();

    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public void preparaEscrita() throws IOException {
        escritor = new FileWriter(file, false);
        impressor = new PrintWriter(escritor);
    }
    
    public void escreveArquivo(String linha){
        impressor.println(linha);
    }

    public void fecharArquivo() {
        impressor.flush();
        impressor.close();
    }
}
