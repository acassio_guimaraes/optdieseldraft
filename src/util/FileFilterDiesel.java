/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.io.File;
import javax.swing.filechooser.FileFilter;

/**
 *
 * @author Acássio
 */
public class FileFilterDiesel extends FileFilter {

    private final String txtFormat = "txt";
    private final char dotIndex = '.';

    @Override
    public boolean accept(File file) {
        if (file.isDirectory()) {
            return true;
        }
        if (extension(file).equalsIgnoreCase(txtFormat)) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String getDescription() {
        return "*.txt";
    }

    public String extension(File file) {
        String fileName = file.getName();
        int indexFile = fileName.lastIndexOf(dotIndex);
        if (indexFile > 0 && indexFile < fileName.length() - 1) {
            return fileName.substring(indexFile + 1);
        } else {
            return "";
        }
    }

}
