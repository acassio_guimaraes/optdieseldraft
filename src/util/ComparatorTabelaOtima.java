/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.util.Comparator;
import model.domain.TabelaOtima;

/**
 *
 * @author Acássio
 */
public class ComparatorTabelaOtima implements Comparator<TabelaOtima> {

    @Override
    public int compare(TabelaOtima o1, TabelaOtima o2) {
        if (o1 == null || o2 == null) {
            return 0;
        }

        //if (o1.getVeic().getCodigoVeiculo().compareTo(o2.getVeic().getCodigoVeiculo()) != 0) {
            return o1.getVeic().getCodigoVeiculo().compareTo(o2.getVeic().getCodigoVeiculo());
        //} else if (o1.getAtiv().getHorarioIncio().after(o2.getAtiv().getHorarioIncio())) {
        //    return 1;
        //} else if (o1.getAtiv().getHorarioIncio().before(o2.getAtiv().getHorarioIncio())) {
        //    return -1;
        //} else {
        //    return 0;
        //}
    }

}
