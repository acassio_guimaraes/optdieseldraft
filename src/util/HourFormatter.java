package util;

import com.towel.bean.Formatter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author Acássio
 */
public class HourFormatter implements Formatter{

    private final static SimpleDateFormat formatter = new SimpleDateFormat("HH:mm");
    
    @Override
    public String format(Object obj) {
        Date dt = (Date) obj;
        return formatter.format(dt);
    }
   
    @Override
    public String getName() {
        return "HourAndMinute";
    }
    
    @Override
    public Object parse(Object s) {
        Date dt = new Date();
        try {
            dt = formatter.parse(s.toString());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dt;
    }

}
