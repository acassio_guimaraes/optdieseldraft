/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author Acássio
 */
public class LeitorTxt {

    private File file;
    
    public LeitorTxt() {
    }

    public LeitorTxt(File file) {
        this.file = file;
    }
    
    public LeitorTxt(String dir, String arq){
        File diretorio = new File(dir);
        this.file = new File(diretorio, arq);
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + Objects.hashCode(this.file);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final LeitorTxt other = (LeitorTxt) obj;
        if (!Objects.equals(this.file, other.file)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "LeitorTxt{" + "file=" + file + '}';
    }
     
    
    public List<String[]> LerArquivoTxt() throws FileNotFoundException, IOException {

        BufferedReader bufferedLeitor;
        List<String[]> resultado;
        
        try (FileReader leitorArquivo = new FileReader(this.file)) {
            bufferedLeitor = new BufferedReader(leitorArquivo);
            getNumberOfLines(this.file);
            String linha;
            String[] linhaSplit;
            resultado = new ArrayList<>();
            while ((linha = bufferedLeitor.readLine()) != null) {
                if (linha != null && !linha.isEmpty()) {
                    linhaSplit = linha.split(";");
                    //System.out.println(linhaSplit[0]);
                    resultado.add(linhaSplit);
                }
            }
        }
        bufferedLeitor.close();
        return resultado;
    }
    
    public int getNumberOfLines(File file) throws FileNotFoundException, IOException{
        LineNumberReader lnr = new LineNumberReader(new FileReader(file));
        lnr.skip(Long.MAX_VALUE);
        return  lnr.getLineNumber();
    }
}
