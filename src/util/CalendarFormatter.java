/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import com.towel.bean.Formatter;

/**
 *
 * @author Acássio
 */
public class CalendarFormatter implements Formatter {

    private final static SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

    @Override
    public String format(Object obj) {
        Calendar cal = (Calendar) obj;
        return formatter.format(cal.getTime());
    }

    @Override
    public String getName() {
        return "calendar";
    }

    @Override
    public Object parse(Object s) {
        Calendar cal = new GregorianCalendar();
        try {
            cal.setTime(formatter.parse(s.toString()));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return cal;
    }
}
