/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import com.towel.el.annotation.AnnotationResolver;
import com.towel.swing.table.ObjectTableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyVetoException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.swing.JOptionPane;
import model.domain.Atividade;
import model.domain.TabelaOtima;
import model.domain.Veiculo;
import util.LeitorTxt;
import view.Principal;
import view.TabelaOtimaView;

/**
 *
 * @author Acássio
 */
public class TebelaOtimaControler implements ActionListener {

    private List<TabelaOtima> tabList;

    private TabelaOtimaView tabView;

    public TebelaOtimaControler(Principal prinView) {

        tabView = new TabelaOtimaView();
        prinView.getJdpPrin().add(tabView);
        tabList = new ArrayList<>();
        atualizarLista();
        ObjectTableModel<TabelaOtima> model = new ObjectTableModel<>(
                new AnnotationResolver(TabelaOtima.class), "veic.codigoVeiculo: Veículo,ativ.nomeLinha: Tarefa,"
                + "ativ.horarioInicio: Horário Início,ativ.horarioFim: Horário fim,"
                + "ativ.distanciaPercorrida: Distância,veic.consumoMedio: Cosumo Med,consumo: Consumo (em litros)"); //, consumo: Consumo (em litros), ativ.distanciaPercorrida: Distância  ",
        //model.setEditableDefault(true);
        model.addAll(tabList);
        tabView.getJpTabOtima().getJtbForm().setModel(model);
        ajustaTela();
        addListeners();
        
    }

    private void ajustaTela() {
        tabView.setResizable(true);
        try {
            tabView.setMaximum(true);
        } catch (PropertyVetoException ex) {
        }
        tabView.setResizable(false);
        tabView.setVisible(true);
    }

    public void atualizarLista() {
        LeitorTxt leitor = new LeitorTxt("C://Users/Acássio/Desktop/testes", "escrita.txt");
        List<String[]> listaArq = new ArrayList<>();
         tabList.clear();
        try {
            listaArq = leitor.LerArquivoTxt();
            for (String[] registro : listaArq) {
                tabList.add(new TabelaOtima(new Atividade(registro[1], registro[3], registro[4], (float) Float.parseFloat(registro[6]), true),
                        new Veiculo(registro[0], "00:00", "00:00", (float) Float.parseFloat(registro[5]), true), (float) Float.parseFloat(registro[2])));
            }
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(tabView, "A tabela de horários ainda não foi otimizada", "Leitura de Arquivo", JOptionPane.INFORMATION_MESSAGE);
        }
        Collections.sort(tabList);
        tabView.getJpTabOtima().getJtbForm().repaint();
    }

    private void addListeners() {
        tabView.getJpTabOtima().getJbSair().addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(tabView.getJpTabOtima().getJbSair())) {
            tabView.setVisible(false);
        }
    }

    public void recuperaTela() {
        tabView.setVisible(true);
        ajustaTela();
    }

    public void perdeTela() {
        tabView.setVisible(false);
    }

}
