/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import com.towel.el.annotation.AnnotationResolver;
import com.towel.swing.table.ObjectTableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyVetoException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import model.domain.Veiculo;
import util.LeitorTxt;
import view.Principal;
import view.VeiculoView;

/**
 *
 * @author Acássio
 */
public class VeiculoControler implements ActionListener {

    private List<Veiculo> veiculoList;

    private VeiculoView veiculoView;

    public VeiculoControler(Principal prinView) {

        veiculoView = new VeiculoView();
        prinView.getJdpPrin().add(veiculoView);
        veiculoList = new ArrayList<>();
        atualizarLista();
        ObjectTableModel<Veiculo> model = new ObjectTableModel<>(
                new AnnotationResolver(Veiculo.class), "codigoVeiculo,horarioInicio,horarioFim,consumoMedio,deficiencia");
        //model.setEditableDefault(true);
        model.addAll(veiculoList);
        veiculoView.getJpVeiculo().getJtbForm().setModel(model);
        ajustaTela();
        addListeners();
    }

    private void ajustaTela() {
        veiculoView.setResizable(true);
        try {
            veiculoView.setMaximum(true);
        } catch (PropertyVetoException ex) {
        }
        veiculoView.setResizable(false);
        veiculoView.setVisible(true);
    }

    private void atualizarLista() {
        LeitorTxt leitor = new LeitorTxt("C://Users/Acássio/Desktop/testes", "veiculos.txt");
        List<String[]> listaArq = new ArrayList<>();
        try {
            listaArq = leitor.LerArquivoTxt();
            for (String[] registro : listaArq) {
                veiculoList.add(new Veiculo(registro[0], registro[1], registro[2],
                        (float) Float.parseFloat(registro[3].replace(",", ".")), (boolean) Boolean.parseBoolean(registro[4])));
            }
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(veiculoView, "Não foi possível ler o arquivo", "Leitura de Arquivo", JOptionPane.INFORMATION_MESSAGE);
        }
    }

    private void addListeners() {
        veiculoView.getJpVeiculo().getJbSair().addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(veiculoView.getJpVeiculo().getJbSair())) {
            veiculoView.setVisible(false);
        }
    }

    public void recuperaTela() {
        veiculoView.setVisible(true);
        ajustaTela();
    }
    
    public void perdeTela(){
        veiculoView.setVisible(false);
    }
}
