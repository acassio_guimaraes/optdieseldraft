package control;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import model.domain.Atividade;
import model.domain.Problema;
import model.domain.Veiculo;
import util.LeitorTxt;

/**
 *
 * @author Acássio
 */
public class Estruturador {

    private int nVeiculos;
    private int nTarefas;
    private List<Veiculo> listaDeVeiculos;
    private List<Atividade> listaDeAtividades;
    private List<Problema> mapaProblema;

    public Estruturador() throws IOException {
        listaDeAtividades = new ArrayList<>();
        listaDeVeiculos = new ArrayList<>();
        mapaProblema        = new ArrayList<>();
        carregarListaVeiculos();
        carregarListaAtividades();
        nVeiculos = listaDeVeiculos.size();
        listaDeAtividades.add(new Atividade("SAIDA"));
        listaDeAtividades.add(new Atividade("RETORNO"));
        nTarefas = listaDeAtividades.size();
    }

    public List<Problema> estruturarMatriz() {
        int count = 1;

        for (int k = 0; k < nVeiculos; ++k) {
            for (int j = 0; j < nTarefas; ++j) {
                for (int i = 0; i < nTarefas; ++i) {
                    if (gerarArco(k, j, i)) {
                        mapaProblema.add(new Problema(k, j, i, count));
                        count++;
                    }
                }
            }
        }
        return mapaProblema;
    }

    // criar duas atividades extras chamadas saida e retorno, para identificar se o ônibus está saindo ou voltando para garagem
    private boolean gerarArco(int k, int j, int i) {

        //Validações triviais 
        // Saí e volta ao mesma tarefa, isso é impossível
        if (i == j) {
            return false;
        }

        //O ponto de saída é o RETORNO ou o ponto de chegada é a Saída
        if (listaDeAtividades.get(i).getNomeLinha().equals("SAIDA")
                || listaDeAtividades.get(j).getNomeLinha().equals("RETORNO")) {
            return false;
        }

        //verifica a atividade de saída: Sair da garagem e realizar uma tarefa
        if (listaDeAtividades.get(j).getNomeLinha().equals("SAIDA")) {
            if (listaDeAtividades.get(i).getNomeLinha().equals("RETORNO")) {
                return false;
            }

            if (!listaDeVeiculos.get(k).isDeficiencia()) {
                if (listaDeAtividades.get(i).isDeficiente()) {
                    return false;
                }
            }

            //Segundo caso, o horário de manutenção impede a segunda tarefa
            if (listaDeVeiculos.get(k).getHorarioInicio().compareTo(listaDeAtividades.get(i).getHorarioIncio()) >= 0
                    & listaDeVeiculos.get(k).getHorarioInicio().compareTo(listaDeAtividades.get(i).getHorarioFim()) <= 0) {
                return false;
            }

            if (listaDeVeiculos.get(k).getHorarioFim().compareTo(listaDeAtividades.get(i).getHorarioIncio()) >= 0
                    & listaDeVeiculos.get(k).getHorarioFim().compareTo(listaDeAtividades.get(i).getHorarioFim()) <= 0) {
                return false;
            }

            return true;
        }

        //verifica a atividade de chegada: Voltar para a garagem
        if (listaDeAtividades.get(i).getNomeLinha().equals("RETORNO")) {
            if (!listaDeVeiculos.get(k).isDeficiencia()) {
                if (listaDeAtividades.get(j).isDeficiente()) {
                    return false;
                }
            }

            //Primeiro caso, o horário de manutenção impede a primeira tarefa
            if (listaDeVeiculos.get(k).getHorarioInicio().compareTo(listaDeAtividades.get(j).getHorarioIncio()) >= 0
                    & listaDeVeiculos.get(k).getHorarioInicio().compareTo(listaDeAtividades.get(j).getHorarioFim()) <= 0) {
                return false;
            }

            if (listaDeVeiculos.get(k).getHorarioFim().compareTo(listaDeAtividades.get(j).getHorarioIncio()) >= 0
                    & listaDeVeiculos.get(k).getHorarioFim().compareTo(listaDeAtividades.get(j).getHorarioFim()) <= 0) {
                return false;
            }

            return true;
        }

        //Valida demais
        //Se o final de uma atividade ocorre no mesmo momento ou depois do início da outra
        //esse arco é inviável
        if (listaDeAtividades.get(j).getHorarioFim().compareTo(listaDeAtividades.get(i).getHorarioIncio()) >= 0) {
            return false;
        }

        //verifica exigência de ônibus adapitado
        if (!listaDeVeiculos.get(k).isDeficiencia()) {
            if (listaDeAtividades.get(j).isDeficiente() || listaDeAtividades.get(i).isDeficiente()) {
                return false;
            }
        }

        //Verifica horário de manutenção dos veículos
        //Primeiro caso, o horário de manutenção impede a primeira tarefa
        if (listaDeVeiculos.get(k).getHorarioInicio().compareTo(listaDeAtividades.get(j).getHorarioIncio()) >= 0
                & listaDeVeiculos.get(k).getHorarioInicio().compareTo(listaDeAtividades.get(j).getHorarioFim()) <= 0) {
            return false;
        }

        if (listaDeVeiculos.get(k).getHorarioFim().compareTo(listaDeAtividades.get(j).getHorarioIncio()) >= 0
                & listaDeVeiculos.get(k).getHorarioFim().compareTo(listaDeAtividades.get(j).getHorarioFim()) <= 0) {
            return false;
        }

        //Segundo caso, o horário de manutenção impede a segunda tarefa
        if (listaDeVeiculos.get(k).getHorarioInicio().compareTo(listaDeAtividades.get(i).getHorarioIncio()) >= 0
                & listaDeVeiculos.get(k).getHorarioInicio().compareTo(listaDeAtividades.get(i).getHorarioFim()) <= 0) {
            return false;
        }

        if (listaDeVeiculos.get(k).getHorarioFim().compareTo(listaDeAtividades.get(i).getHorarioIncio()) >= 0
                & listaDeVeiculos.get(k).getHorarioFim().compareTo(listaDeAtividades.get(i).getHorarioFim()) <= 0) {
            return false;
        }

        //se passou por tudo retorna verdadeiro
        return true;
    }

    private void carregarListaVeiculos() throws IOException {
        LeitorTxt leitor = new LeitorTxt("C://Users/Acássio/Desktop/testes", "veiculos.txt");
        List<String[]> listaArq = new ArrayList<>();
        listaArq = leitor.LerArquivoTxt();
        for (String[] registro : listaArq) {
            listaDeVeiculos.add(new Veiculo(registro[0], registro[1], registro[2],
                    (float) Float.parseFloat(registro[3].replace(",", ".")), (boolean) Boolean.parseBoolean(registro[4])));
        }
    }

    private void carregarListaAtividades() throws IOException {
        LeitorTxt leitor = new LeitorTxt("C://Users/Acássio/Desktop/testes", "linhas.txt");
        List<String[]> listaArq = new ArrayList<>();
        listaArq = leitor.LerArquivoTxt();
        for (String[] registro : listaArq) {
            listaDeAtividades.add(new Atividade(registro[0], registro[1], registro[2],
                    (float) Float.parseFloat(registro[3].replace(",", ".")), (boolean) Boolean.parseBoolean(registro[4])));
        }
    }

    public int getnVeiculos() {
        return nVeiculos;
    }

    public int getnTarefas() {
        return nTarefas;
    }

    public List<Veiculo> getListaDeVeiculos() {
        return listaDeVeiculos;
    }

    public List<Atividade> getListaDeAtividades() {
        return listaDeAtividades;
    }
}
