/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import com.towel.el.annotation.AnnotationResolver;
import com.towel.swing.table.ObjectTableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyVetoException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import model.domain.Atividade;
import util.LeitorTxt;
import view.AtividadeView;
import view.Principal;

/**
 *
 * @author Acássio
 */
public class AtividadeControler implements ActionListener {

    private List<Atividade> ativList;

    private AtividadeView atiView;

    public AtividadeControler(Principal prinView) {

        atiView = new AtividadeView();
        prinView.getJdpPrin().add(atiView);
        ativList = new ArrayList<>();
        atualizarLista();
        ObjectTableModel<Atividade> model = new ObjectTableModel<>(
                new AnnotationResolver(Atividade.class), "nomeLinha,horarioInicio,horarioFim,distanciaPercorrida,deficiente");
        //model.setEditableDefault(true);
        model.addAll(ativList);
        atiView.getJpAtividade().getJtbForm().setModel(model);
        ajustaTela();
        addListeners();
    }

    private void ajustaTela() {
        atiView.setResizable(true);
        try {
            atiView.setMaximum(true);
        } catch (PropertyVetoException ex) {
        }
        atiView.setResizable(false);
        atiView.setVisible(true);
    }

    private void atualizarLista() {
        LeitorTxt leitor = new LeitorTxt("C://Users/Acássio/Desktop/testes", "linhas.txt");
        List<String[]> listaArq = new ArrayList<>();
        try {
            listaArq = leitor.LerArquivoTxt();
            for (String[] registro : listaArq) {
                ativList.add(new Atividade(registro[0], registro[1], registro[2],
                        (float) Float.parseFloat(registro[3].replace(",", ".")), (boolean) Boolean.parseBoolean(registro[4])));
            }
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(atiView, "Não foi possível ler o arquivo", "Leitura de Arquivo", JOptionPane.INFORMATION_MESSAGE);
        }
    }

    private void addListeners() {
        atiView.getJpAtividade().getJbSair().addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(atiView.getJpAtividade().getJbSair())) {
            atiView.setVisible(false);
        }
    }

    public void recuperaTela() {
        atiView.setVisible(true);
        ajustaTela();
    }

    public void perdeTela() {
        atiView.setVisible(false);
    }
}
