/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import lpsolve.LpSolve;
import lpsolve.LpSolveException;
import model.domain.Problema;
import util.EscritorTxt;

/**
 *
 * @author Acássio
 */
public class Solver {

    private List<Problema> mapaProblema;
    private int nVeiculos;
    private int nTarefas;
    private Estruturador estr;
    private double[] row;

    public Solver() {

    }

    public void gerarMatriz() throws IOException {
        estr = new Estruturador();
        mapaProblema = estr.estruturarMatriz();
        nVeiculos = estr.getnVeiculos();
        nTarefas = estr.getnTarefas();
    }

    public void estruturarProblema() throws LpSolveException {
        LpSolve lp;
        int nCol;
        int nRestri;
        int count;
        int ret = 0;

        nCol = mapaProblema.size();

        //número de restrições do problema nTar*nVeiculos = número de restrições de fluxo
        //temos mais nVeiculos restrições de veículo do tipo k que saem da garagem
        //e mais nTarefas restrições de atribuição, ou seja uma tarefa é realizado por apenas um veículo de uma vez
        // o -2 está excluindo as tarefas de saída e chegada na garagem
        nRestri = (nTarefas - 2) * nVeiculos + nVeiculos + nTarefas - 2;

        int[] colNo = new int[nCol];
        row = new double[nCol];

        lp = LpSolve.makeLp(0, nCol);

        if (lp.getLp() == 0) {
            ret = 1;
        }

        if (ret == 0) {

            lp.setAddRowmode(true);

            int countVeiculo = 0;
            int countPartida = 0;
            int countChegada = 0;
            int countVeiculoFluxo = 0;

            for (int k = 1; k <= nRestri; k++) {
                count = 0;
                if (k > nTarefas + nVeiculos - 2) {//restrições de fluxo
                    for (int i = 0; i < nCol; i++) {
                        //if (!estr.getListaDeAtividades().get(mapaProblema.get(i).getPosTarFin()).getNomeLinha().equals("SAIDA")) {
                        //if (!estr.getListaDeAtividades().get(mapaProblema.get(i).getPosTarFin()).getNomeLinha().equals("RETORNO")) {
                        if (mapaProblema.get(i).getPosVeiculo() == countVeiculoFluxo) {
                            if (mapaProblema.get(i).getPosTarFin() == countChegada) {
                                colNo[count] = mapaProblema.get(i).getnVariavel();
                                row[count] = 1;
                                //                            System.out.println("Contador: " + count + " ColNo: " + colNo[count] + " Row: " + row[count]
                                //                                    + " Interção: " + k + " k = " + mapaProblema.get(i).getPosVeiculo()
                                //                                    + " j = " + mapaProblema.get(i).getPosTarIni() + " i = " + mapaProblema.get(i).getPosTarFin()
                                //                                    + " FLUXO! -- Chegad: " + countChegada + " Veiculo: " + countVeiculoFluxo);
                                count++;
                            }
                            if (mapaProblema.get(i).getPosTarIni() == countChegada) {
                                colNo[count] = mapaProblema.get(i).getnVariavel();
                                row[count] = -1;
                                //                              System.out.println("Contador: " + count + " ColNo: " + colNo[count] + " Row: " + row[count]
                                //                                      + " Interção: " + k + " k = " + mapaProblema.get(i).getPosVeiculo()
                                //                                      + " j = " + mapaProblema.get(i).getPosTarIni() + " i = " + mapaProblema.get(i).getPosTarFin()
                                //                                      + " FLUXO! -- Chegad: " + countChegada + " Veiculo: " + countVeiculoFluxo);
                                count++;
                            }
                        }
                        //}
                        //}
                    }
                    countChegada++;
                    if (countChegada == nTarefas - 2) {
                        countVeiculoFluxo++;
                        countChegada = 0;
                    }
                    if (count > 0) {
                        lp.addConstraintex(count, row, colNo, LpSolve.EQ, 0);
                    }
                } else if (k > nTarefas - 2 & k <= nTarefas + nVeiculos - 2) { //retrições de número de veículos
                    for (int i = 0; i < nCol; i++) {
                        if (estr.getListaDeAtividades().get(mapaProblema.get(i).getPosTarIni()).getNomeLinha().equals("SAIDA")) {
                            if (mapaProblema.get(i).getPosVeiculo() == countVeiculo) {
                                colNo[count] = mapaProblema.get(i).getnVariavel();
                                row[count] = 1;
                                //                              System.out.println("Contador: " + count + " ColNo: " + colNo[count] + " Row: " + row[count]
                                //                                      + " Interção: " + k + " k = " + mapaProblema.get(i).getPosVeiculo()
                                //                                      + " j = " + mapaProblema.get(i).getPosTarIni() + " i = " + mapaProblema.get(i).getPosTarFin()
                                //                                     + " N VEIC!");
                                count++;

                            }
                        }
                    }
                    if (count > 0) {
                        lp.addConstraintex(count, row, colNo, LpSolve.LE, 1);
                    }
                    countVeiculo++;
                } else {//retrições de atribuição 
                    for (int i = 0; i < nCol; i++) {
                        if (mapaProblema.get(i).getPosTarIni() == countPartida) {
                            if (!estr.getListaDeAtividades().get(mapaProblema.get(i).getPosTarIni()).getNomeLinha().equals("SAIDA")) {
                                if (!estr.getListaDeAtividades().get(mapaProblema.get(i).getPosTarIni()).getNomeLinha().equals("RETORNO")) {
                                    colNo[count] = mapaProblema.get(i).getnVariavel();
                                    row[count] = 1;
                                    //                                  System.out.println("Contador: " + count + " ColNo: " + colNo[count] + " Row: " + row[count]
                                    //                                          + " Interção: " + k + " k = " + mapaProblema.get(i).getPosVeiculo()
                                    //                                          + " j = " + mapaProblema.get(i).getPosTarIni() + " i = " + mapaProblema.get(i).getPosTarFin()
                                    //                                         + " ATRIB!");
                                    count++;
                                }
                            }
                        }
                    }
                    if (count > 0) {
                        lp.addConstraintex(count, row, colNo, LpSolve.EQ, 1);
                    }
                    countPartida++;
                }

            }
            lp.setAddRowmode(false);
            //restrição de integralidade das variáveis
            //definição da função objetivo

            nCol = mapaProblema.size();
            count = 0;
            for (int i = 0; i < nCol; i++) {
                lp.setInt(mapaProblema.get(i).getnVariavel(), true);
                if (!estr.getListaDeAtividades().get(mapaProblema.get(i).getPosTarFin()).getNomeLinha().equals("RETORNO")) {
                    colNo[count++] = mapaProblema.get(i).getnVariavel();
                    row[count] = estr.getListaDeAtividades().get(mapaProblema.get(i).getPosTarFin()).getDistanciaPercorrida()
                            * estr.getListaDeVeiculos().get(mapaProblema.get(i).getPosVeiculo()).getConsumoMedio();
                }
            }
            lp.setObjFnex(count, row, colNo);
            lp.setMinim();
            //lp.setMaxim();

            //          System.out.println("O número de colunas do problema é: " + lp.getNcolumns());
            //          System.out.println("O nome da colina 1 é: " + lp.getColName(1));
            ret = lp.solve();
            if (ret == LpSolve.OPTIMAL) {
                ret = 0;
            } else {
                ret = 5;
            }

//            System.out.println("O valor de ret é:" + ret);
            if (ret == 0) {
                /* a solution is calculated, now lets get some results */

                /* variable values */
                lp.getVariables(row);
                //for (int j = 0; j < nCol; j++) {
                //    System.out.println(lp.getColName(j + 1) + ": " + row[j] + " -- " + "k = " + mapaProblema.get(j).getPosVeiculo()
                //            + " j = " + mapaProblema.get(j).getPosTarIni() + " i = " + mapaProblema.get(j).getPosTarFin());
                //}

            }
        }
    }

    public void salvarResultados() throws IOException {
        int size = row.length;
        String nomeVeiculo;
        String nomeTarefa;
        float consumo;
        Date horInicio;
        Date horFim;
        String inicio;
        String fim;
        float quilometragem;
        float consumoMedio;
        SimpleDateFormat formatter = new SimpleDateFormat("HH:mm");
        EscritorTxt escTxt;
        escTxt = new EscritorTxt("C://Users/Acássio/Desktop/testes", "escrita.txt");
        escTxt.preparaEscrita();
        String linha;
        for (int j = 0; j < size; j++) {
            if (row[j] == 1) {
                if (!estr.getListaDeAtividades().get(mapaProblema.get(j).getPosTarFin()).getNomeLinha().equals("RETORNO")) {
                    nomeVeiculo = estr.getListaDeVeiculos().get(mapaProblema.get(j).getPosVeiculo()).getCodigoVeiculo();
                    nomeTarefa = estr.getListaDeAtividades().get(mapaProblema.get(j).getPosTarFin()).getNomeLinha();
                    consumo = estr.getListaDeVeiculos().get(mapaProblema.get(j).getPosVeiculo()).getConsumoMedio()
                            * estr.getListaDeAtividades().get(mapaProblema.get(j).getPosTarFin()).getDistanciaPercorrida();
                    consumoMedio = estr.getListaDeVeiculos().get(mapaProblema.get(j).getPosVeiculo()).getConsumoMedio();
                    quilometragem = estr.getListaDeAtividades().get(mapaProblema.get(j).getPosTarFin()).getDistanciaPercorrida();
                    inicio = formatter.format(estr.getListaDeAtividades().get(mapaProblema.get(j).getPosTarFin()).getHorarioIncio());
                    fim = formatter.format(estr.getListaDeAtividades().get(mapaProblema.get(j).getPosTarFin()).getHorarioFim());
                    linha = nomeVeiculo + ";" + nomeTarefa + ";" + consumo + ";" + inicio + ";" + fim + ";" + consumoMedio + ";" + quilometragem;
                    escTxt.escreveArquivo(linha);
                }
            }
        }
        escTxt.fecharArquivo();
    }

}
