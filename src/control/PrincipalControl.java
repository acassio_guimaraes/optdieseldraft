package control;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;
import java.util.List;
import javax.swing.JOptionPane;
import lpsolve.LpSolveException;
import model.domain.Problema;
import view.Principal;

/**
 *
 * @author Acássio
 */
public class PrincipalControl implements ActionListener, MouseListener, KeyListener {

    private Principal prinView;
    private AtividadeControler ativControl = null;
    private VeiculoControler veiculoControl = null;
    private TebelaOtimaControler tabControl = null;
    private Estruturador estr;
    private List<Problema> listProblem;
    private Solver slv;

    public PrincipalControl() {
        prinView = new Principal();
        prinView.setVisible(true);
        prinView.setResizable(false);
        addListeners();
    }

    private void addListeners() {
        this.prinView.getJtrPrin().addMouseListener(this);
        this.prinView.getJtrPrin().addKeyListener(this);
        this.prinView.getJbVeriProblema().addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(prinView.getJbVeriProblema())) {
            try {
                estr = new Estruturador();

                listProblem = estr.estruturarMatriz();
                slv = new Solver();

                slv.gerarMatriz();
            } catch (IOException ex) {

            }

            try {
                slv.estruturarProblema();
                slv.salvarResultados();
               JOptionPane.showMessageDialog(this.prinView, "Tabela otimizada gerada!", "Resultado", JOptionPane.INFORMATION_MESSAGE);
            } catch (LpSolveException | IOException ex) {

            }

        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        //Atividades
        if (e.getSource().equals(prinView.getJtrPrin())) {
            if ("Linhas e Horários".equals(prinView.getJtrPrin().getLastSelectedPathComponent().toString())) {
                if (ativControl == null) {
                    ativControl = new AtividadeControler(prinView);
                } else {
                    if (veiculoControl != null) {
                        veiculoControl.perdeTela();
                    }
                    if (tabControl != null) {
                        tabControl.perdeTela();
                    }
                    ativControl.recuperaTela();
                }

            }
        }

        //Veículos 
        if (e.getSource().equals(prinView.getJtrPrin())) {
            if ("Veículos".equals(prinView.getJtrPrin().getLastSelectedPathComponent().toString())) {
                if (veiculoControl == null) {
                    veiculoControl = new VeiculoControler(prinView);
                } else {
                    if (ativControl != null) {
                        ativControl.perdeTela();
                    }
                    if (tabControl != null) {
                        tabControl.perdeTela();
                    }
                    veiculoControl.recuperaTela();
                }
            }
        }

        //Resultados 
        if (e.getSource().equals(prinView.getJtrPrin())) {
            if ("Resultados".equals(prinView.getJtrPrin().getLastSelectedPathComponent().toString())) {
                if (tabControl == null) {
                    tabControl = new TebelaOtimaControler(prinView);
                } else {
                    if (ativControl != null) {
                        ativControl.perdeTela();
                    }
                    if (veiculoControl != null) {
                        veiculoControl.perdeTela();
                    }
                    tabControl.atualizarLista();
                    tabControl.recuperaTela();
                }
            }
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
    }

    @Override
    public void keyReleased(KeyEvent e) {
    }

}
