package model.domain;

import com.towel.el.annotation.Resolvable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;
import util.HourFormatter;

/**
 *
 * @author Acássio
 */
public class Atividade {

    //Nome da linha

    @Resolvable(colName = "Linha") // Projeto Towel, isso gerará a Jtable via annotations
    private String nomeLinha;
    //Horário de Início da atividade 
    @Resolvable(colName = "Hor. Início", formatter = HourFormatter.class)
    private Date horarioInicio;
    //Horário de fim da atividade
    @Resolvable(colName = "Hor. Fim", formatter = HourFormatter.class)
    private Date horarioFim;
    //Distancia percorrida entre o ponto incial e final
    @Resolvable(colName = "Distância")
    private float distanciaPercorrida;
    //Identifica se o ônibus que vai atender essa linha precisa ser 
    //adaptado para atender pessoas com deficiência
    @Resolvable(colName = "Adaptado")
    private boolean deficiente;

    public Atividade() {
    }
    
    public Atividade(String nomeLinha) {
        this.nomeLinha = nomeLinha;
    }

    public Atividade(String nomeLinha, String horarioInicio, String horarioFim, float distanciaPercorrida, boolean deficiente) {
        this.nomeLinha = nomeLinha;
        this.distanciaPercorrida = distanciaPercorrida;
        this.deficiente = deficiente;
        this.horarioInicio = new Date();
        this.horarioFim = new Date();
        try {
            this.horarioInicio = new SimpleDateFormat("HH:mm").parse(horarioInicio);
            this.horarioFim = new SimpleDateFormat("HH:mm").parse(horarioFim);
        } catch (ParseException ex) {
        }
    }

    public String getNomeLinha() {
        return nomeLinha;
    }

    public void setNomeLinha(String nomeLinha) {
        this.nomeLinha = nomeLinha;
    }

    public Date getHorarioIncio() {
        return horarioInicio;
    }

    public void setHorarioIncio(Date horarioIncio) {
        this.horarioInicio = horarioIncio;
    }

    public Date getHorarioFim() {
        return horarioFim;
    }

    public void setHorarioFim(Date horarioFim) {
        this.horarioFim = horarioFim;
    }

    public float getDistanciaPercorrida() {
        return distanciaPercorrida;
    }

    public void setDistanciaPercorrida(float distanciaPercorrida) {
        this.distanciaPercorrida = distanciaPercorrida;
    }

    public boolean isDeficiente() {
        return deficiente;
    }

    public void setDeficiente(boolean deficiente) {
        this.deficiente = deficiente;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 89 * hash + Objects.hashCode(this.nomeLinha);
        hash = 89 * hash + Objects.hashCode(this.horarioInicio);
        hash = 89 * hash + Objects.hashCode(this.horarioFim);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Atividade other = (Atividade) obj;
        if (!Objects.equals(this.nomeLinha, other.nomeLinha)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Atividade{" + "nomeLinha=" + nomeLinha + ", horarioIncio=" + horarioInicio + ", horarioFim=" + horarioFim + ", distanciaPercorrida=" + distanciaPercorrida + ", deficiente=" + deficiente + '}';
    }
}
