/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.domain;

/**
 *
 * @author Acássio
 */
public class Problema {

    private int posVeiculo;
    private int posTarIni;
    private int posTarFin;
    private int nVariavel;

    public Problema() {
    }

    public Problema(int posVeiculo, int posTarIni, int posTarFin, int nVariavel) {
        this.posVeiculo = posVeiculo;
        this.posTarIni = posTarIni;
        this.posTarFin = posTarFin;
        this.nVariavel = nVariavel;
    }

    public int getPosVeiculo() {
        return posVeiculo;
    }

    public void setPosVeiculo(int posVeiculo) {
        this.posVeiculo = posVeiculo;
    }

    public int getPosTarIni() {
        return posTarIni;
    }

    public void setPosTarIni(int posTarIni) {
        this.posTarIni = posTarIni;
    }

    public int getPosTarFin() {
        return posTarFin;
    }

    public void setPosTarFin(int posTarFin) {
        this.posTarFin = posTarFin;
    }

    public int getnVariavel() {
        return nVariavel;
    }

    public void setnVariavel(int nVariavel) {
        this.nVariavel = nVariavel;
    }
}
