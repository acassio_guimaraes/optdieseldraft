package model.domain;

import com.towel.el.annotation.Resolvable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;
import util.HourFormatter;

/**
 *
 * @author Acássio
 */
public class Veiculo {

    //Identificador do Veículo
    @Resolvable(colName = "Veículo") // Projeto Towel, isso gerará a Jtable via annotations
    private String codigoVeiculo;
    //Horário de início da indisponibilidade
    @Resolvable(colName = "Hor. Início Indisp", formatter = HourFormatter.class)
    private Date horarioInicio;
    //Horário de fim da indisponibilidade
    @Resolvable(colName = "Hor. Fim Indisp", formatter = HourFormatter.class)
    private Date horarioFim;
    //Consumo médio do veículo
    @Resolvable(colName = "Consumo (litros/Km)")
    private float consumoMedio;
    //Variável que identifica se o veículo é capaz de atender
    //pessoas com deficiência
    @Resolvable(colName = "Adaptado")
    private boolean deficiencia;

    public Veiculo() {
    }

    public Veiculo(String codigoVeiculo, String horarioInicio, String horarioFim, float consumoMedio, boolean deficiencia) {
        this.codigoVeiculo = codigoVeiculo;
        this.consumoMedio = consumoMedio;
        this.deficiencia = deficiencia;
        this.horarioInicio = new Date();
        this.horarioFim = new Date();
        try {
            this.horarioInicio = new SimpleDateFormat("HH:mm").parse(horarioInicio);
            this.horarioFim = new SimpleDateFormat("HH:mm").parse(horarioFim);
        } catch (ParseException ex) {
        }

    }

    public String getCodigoVeiculo() {
        return codigoVeiculo;
    }

    public void setCodigoVeiculo(String codigoVeiculo) {
        this.codigoVeiculo = codigoVeiculo;
    }

    public Date getHorarioInicio() {
        return horarioInicio;
    }

    public void setHorarioInicio(Date horarioInicio) {
        this.horarioInicio = horarioInicio;
    }

    public Date getHorarioFim() {
        return horarioFim;
    }

    public void setHorarioFim(Date horarioFim) {
        this.horarioFim = horarioFim;
    }

    public float getConsumoMedio() {
        return consumoMedio;
    }

    public void setConsumoMedio(float consumoMedio) {
        this.consumoMedio = consumoMedio;
    }

    public boolean isDeficiencia() {
        return deficiencia;
    }

    public void setDeficiencia(boolean deficiencia) {
        this.deficiencia = deficiencia;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + Objects.hashCode(this.codigoVeiculo);
        hash = 89 * hash + (this.deficiencia ? 1 : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Veiculo other = (Veiculo) obj;
        if (!Objects.equals(this.codigoVeiculo, other.codigoVeiculo)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Veiculo{" + "codigoVeiculo=" + codigoVeiculo + ", horarioInicio=" + horarioInicio + ", horarioFim=" + horarioFim + ", consumoMedio=" + consumoMedio + ", deficiencia=" + deficiencia + '}';
    }

}
