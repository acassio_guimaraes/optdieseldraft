package model.domain;

/**
 *
 * @author Acássio
 */
public class Projeto {
private String projectName;
private String arqVeiculos;
private String arqTarefas;
private String arqResult;

    public Projeto(String projectName, String arqVeiculos, String arqTarefas, String arqResult) {
        this.projectName = projectName;
        this.arqVeiculos = arqVeiculos;
        this.arqTarefas = arqTarefas;
        this.arqResult = arqResult;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getArqVeiculos() {
        return arqVeiculos;
    }

    public void setArqVeiculos(String arqVeiculos) {
        this.arqVeiculos = arqVeiculos;
    }

    public String getArqTarefas() {
        return arqTarefas;
    }

    public void setArqTarefas(String arqTarefas) {
        this.arqTarefas = arqTarefas;
    }

    public String getArqResult() {
        return arqResult;
    }

    public void setArqResult(String arqResult) {
        this.arqResult = arqResult;
    }

}
