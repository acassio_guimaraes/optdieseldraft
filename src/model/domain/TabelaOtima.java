/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.domain;

import com.towel.el.annotation.Resolvable;
import java.util.Objects;

/**
 *
 * @author Acássio
 */
public class TabelaOtima implements Comparable<TabelaOtima> {

    private Atividade ativ;
    private Veiculo veic;
    @Resolvable(colName = "Consumo")
    private float consumo;

    public TabelaOtima(Atividade ativ, Veiculo veic) {
        this.ativ = ativ;
        this.veic = veic;
        this.consumo = veic.getConsumoMedio() * ativ.getDistanciaPercorrida();
    }

    public TabelaOtima(Atividade ativ, Veiculo veic, float consumo) {
        this.ativ = ativ;
        this.veic = veic;
        this.consumo = consumo;
    }

    public Atividade getAtiv() {
        return ativ;
    }

    public void setAtiv(Atividade ativ) {
        this.ativ = ativ;
    }

    public Veiculo getVeic() {
        return veic;
    }

    public void setVeic(Veiculo veic) {
        this.veic = veic;
    }

    public float getConsumo() {
        return consumo;
    }

    public void setConsumo(float consumo) {
        this.consumo = consumo;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.ativ);
        hash = 97 * hash + Objects.hashCode(this.veic);
        hash = 97 * hash + Float.floatToIntBits(this.consumo);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TabelaOtima other = (TabelaOtima) obj;
        if (!Objects.equals(this.ativ, other.ativ)) {
            return false;
        }
        if (!Objects.equals(this.veic, other.veic)) {
            return false;
        }
        if (Float.floatToIntBits(this.consumo) != Float.floatToIntBits(other.consumo)) {
            return false;
        }
        return true;
    }

    @Override
    public int compareTo(TabelaOtima outraTab) {
        if (outraTab == null) {
            return 1;
        }

        if (this.getVeic().getCodigoVeiculo().compareTo(outraTab.getVeic().getCodigoVeiculo()) != 0) {
            if (this.getVeic().getCodigoVeiculo().compareTo(outraTab.getVeic().getCodigoVeiculo()) > 0) {
                return 1;
            } else {
                return -1;
            }
        } else {
            if (this.getAtiv().getHorarioIncio().after(outraTab.getAtiv().getHorarioIncio())) {
                return 1;
            } else {
                return - 1;
            }

        }
    }
}
