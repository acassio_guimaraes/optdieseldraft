package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;

/**
 *
 * @author Acássio
 */
public class JPanelPadrao extends JPanel {

    private JButton jbBuscar;
    private JButton jbExcluir;
    private JButton jbImportar;
    private JButton jbNovo;
    private JButton jbSair;
    private JButton jbSalvar;
    private JPanel jpBotoes;
    private JPanel jpCamposPrimeiro;
    private JPanel jpCamposSegundo;
    private JPanel jpCamposTerceiro;
    private JPanel jpExternoCampos;
    private JPanel jpInterTabela;
    private JPanel jpTabela;
    private JScrollPane jspForm;
    private JTable jtbForm;

    public JPanelPadrao() {

        jpBotoes = new JPanel();
        jbNovo = new JButton();
        jbImportar = new JButton();
        jbSalvar = new JButton();
        jbBuscar = new JButton();
        jbExcluir = new JButton();
        jbSair = new JButton();
        jpExternoCampos = new JPanel();
        jpCamposPrimeiro = new JPanel();
        jpCamposSegundo = new JPanel();
        jpCamposTerceiro = new JPanel();
        jpTabela = new JPanel();
        jpInterTabela = new JPanel();
        jspForm = new JScrollPane();
        jtbForm = new JTable();

        setLayout(new BorderLayout());
        jpBotoes.setBorder(BorderFactory.createEtchedBorder());
        jpBotoes.setPreferredSize(new Dimension(100, 418));
        FlowLayout flowLayout4 = new FlowLayout(FlowLayout.LEFT, 2, 20);
        flowLayout4.setAlignOnBaseline(true);
        jpBotoes.setLayout(flowLayout4);

        jbNovo.setFont(new Font("Arial", 1, 12)); 
        jbNovo.setForeground(new Color(153, 153, 153));
        jbNovo.setIcon(new ImageIcon(getClass().getResource("/icons/Add Row-32 (2).png"))); 
        jbNovo.setText("Novo");
        jbNovo.setBorder(null);
        jbNovo.setBorderPainted(false);
        jbNovo.setContentAreaFilled(false);
        jbNovo.setCursor(new Cursor(Cursor.HAND_CURSOR));
        jbNovo.setFocusPainted(false);
        jbNovo.setHorizontalAlignment(SwingConstants.RIGHT);
        jbNovo.setHorizontalTextPosition(SwingConstants.LEFT);
        jbNovo.setPreferredSize(new Dimension(85, 33));
        jbNovo.setPressedIcon(new ImageIcon(getClass().getResource("/icons/Add Row-32 (1).png")));
        jpBotoes.add(jbNovo);

        jbImportar.setFont(new Font("Arial", 1, 12)); 
        jbImportar.setForeground(new Color(153, 153, 153));
        jbImportar.setIcon(new ImageIcon(getClass().getResource("/icons/Add Database-32.png"))); 
        jbImportar.setText("Importar");
        jbImportar.setBorder(null);
        jbImportar.setBorderPainted(false);
        jbImportar.setContentAreaFilled(false);
        jbImportar.setCursor(new Cursor(Cursor.HAND_CURSOR));
        jbImportar.setFocusPainted(false);
        jbImportar.setHorizontalAlignment(SwingConstants.RIGHT);
        jbImportar.setHorizontalTextPosition(SwingConstants.LEFT);
        jbImportar.setPreferredSize(new Dimension(85, 33));
        jbImportar.setPressedIcon(new ImageIcon(getClass().getResource("/icons/Add Database-32 (1).png"))); 
        jpBotoes.add(jbImportar);

        jbSalvar.setFont(new Font("Arial", 1, 12)); // NOI18N
        jbSalvar.setForeground(new Color(153, 153, 153));
        jbSalvar.setIcon(new ImageIcon(getClass().getResource("/icons/Save-32.png"))); 
        jbSalvar.setText("Salvar");
        jbSalvar.setToolTipText("");
        jbSalvar.setBorder(null);
        jbSalvar.setBorderPainted(false);
        jbSalvar.setContentAreaFilled(false);
        jbSalvar.setCursor(new Cursor(Cursor.HAND_CURSOR));
        jbSalvar.setFocusPainted(false);
        jbSalvar.setHorizontalAlignment(SwingConstants.RIGHT);
        jbSalvar.setHorizontalTextPosition(SwingConstants.LEFT);
        jbSalvar.setInheritsPopupMenu(true);
        jbSalvar.setPreferredSize(new Dimension(85, 33));
        jbSalvar.setPressedIcon(new ImageIcon(getClass().getResource("/icons/Save-32 (1).png"))); 
        jpBotoes.add(jbSalvar);

        jbBuscar.setFont(new Font("Arial", 1, 12)); 
        jbBuscar.setForeground(new Color(153, 153, 153));
        jbBuscar.setIcon(new ImageIcon(getClass().getResource("/icons/Search-32.png")));
        jbBuscar.setText("Buscar");
        jbBuscar.setBorder(null);
        jbBuscar.setBorderPainted(false);
        jbBuscar.setContentAreaFilled(false);
        jbBuscar.setCursor(new Cursor(Cursor.HAND_CURSOR));
        jbBuscar.setFocusPainted(false);
        jbBuscar.setHorizontalAlignment(SwingConstants.RIGHT);
        jbBuscar.setHorizontalTextPosition(SwingConstants.LEFT);
        jbBuscar.setPreferredSize(new Dimension(85, 33));
        jbBuscar.setPressedIcon(new ImageIcon(getClass().getResource("/icons/Search-32 (1).png")));
        jpBotoes.add(jbBuscar);

        jbExcluir.setFont(new Font("Arial", 1, 12));
        jbExcluir.setForeground(new Color(153, 153, 153));
        jbExcluir.setIcon(new ImageIcon(getClass().getResource("/icons/Delete Sign-32.png")));
        jbExcluir.setText("Excluir");
        jbExcluir.setToolTipText("");
        jbExcluir.setBorder(null);
        jbExcluir.setBorderPainted(false);
        jbExcluir.setContentAreaFilled(false);
        jbExcluir.setCursor(new Cursor(Cursor.HAND_CURSOR));
        jbExcluir.setFocusPainted(false);
        jbExcluir.setHorizontalAlignment(SwingConstants.RIGHT);
        jbExcluir.setHorizontalTextPosition(SwingConstants.LEFT);
        jbExcluir.setPreferredSize(new Dimension(85, 33));
        jbExcluir.setPressedIcon(new ImageIcon(getClass().getResource("/icons/Delete Sign-32 (1).png")));
        jpBotoes.add(jbExcluir);

        jbSair.setFont(new Font("Arial", 1, 12));
        jbSair.setForeground(new Color(153, 153, 153));
        jbSair.setIcon(new ImageIcon(getClass().getResource("/icons/Exit Sign-32.png")));
        jbSair.setText("Sair");
        jbSair.setToolTipText("");
        jbSair.setBorder(null);
        jbSair.setBorderPainted(false);
        jbSair.setContentAreaFilled(false);
        jbSair.setCursor(new Cursor(Cursor.HAND_CURSOR));
        jbSair.setFocusPainted(false);
        jbSair.setHorizontalAlignment(SwingConstants.RIGHT);
        jbSair.setHorizontalTextPosition(SwingConstants.LEFT);
        jbSair.setPreferredSize(new Dimension(85, 33));
        jbSair.setPressedIcon(new ImageIcon(getClass().getResource("/icons/Exit Sign-32 (1).png")));
        jpBotoes.add(jbSair);

        add(jpBotoes, BorderLayout.LINE_START);
        jpExternoCampos.setBorder(BorderFactory.createEtchedBorder());
        jpExternoCampos.setLayout(new BoxLayout(jpExternoCampos, BoxLayout.Y_AXIS));

        FlowLayout flowLayout1 = new FlowLayout(FlowLayout.LEFT);
        flowLayout1.setAlignOnBaseline(true);
        jpCamposPrimeiro.setLayout(flowLayout1);

        jpExternoCampos.add(jpCamposPrimeiro);

        FlowLayout flowLayout2 = new FlowLayout(FlowLayout.LEFT);
        flowLayout2.setAlignOnBaseline(true);
        jpCamposSegundo.setLayout(flowLayout2);
        jpExternoCampos.add(jpCamposSegundo);

        FlowLayout flowLayout3 = new FlowLayout(FlowLayout.LEFT);
        flowLayout3.setAlignOnBaseline(true);
        jpCamposTerceiro.setLayout(flowLayout3);
        jpExternoCampos.add(jpCamposTerceiro);

        add(jpExternoCampos, BorderLayout.PAGE_START);
        jpTabela.setLayout(new BorderLayout());

        jpInterTabela.setPreferredSize(new Dimension(734, 240));
        jpInterTabela.setLayout(new BoxLayout(jpInterTabela, BoxLayout.LINE_AXIS));

        jtbForm.setFont(new Font("Arial", 0, 12));
        jtbForm.setOpaque(false);
        jtbForm.setRowHeight(25);
        jtbForm.setSelectionBackground(new Color(204, 255, 204));
        jtbForm.setShowHorizontalLines(false);
        jspForm.setViewportView(jtbForm);

        jpInterTabela.add(jspForm);

        jpTabela.add(jpInterTabela, BorderLayout.CENTER);

        add(jpTabela, BorderLayout.CENTER);
    }

    public JButton getJbBuscar() {
        return jbBuscar;
    }

    public JButton getJbExcluir() {
        return jbExcluir;
    }

    public JButton getJbImportar() {
        return jbImportar;
    }

    public JButton getJbNovo() {
        return jbNovo;
    }

    public JButton getJbSair() {
        return jbSair;
    }

    public JButton getJbSalvar() {
        return jbSalvar;
    }

    public JPanel getJpBotoes() {
        return jpBotoes;
    }

    public JPanel getJpCamposPrimeiro() {
        return jpCamposPrimeiro;
    }

    public JPanel getJpCamposSegundo() {
        return jpCamposSegundo;
    }

    public JPanel getJpCamposTerceiro() {
        return jpCamposTerceiro;
    }

    public JPanel getJpExternoCampos() {
        return jpExternoCampos;
    }

    public JPanel getJpInterTabela() {
        return jpInterTabela;
    }

    public JPanel getJpTabela() {
        return jpTabela;
    }

    public JScrollPane getJspForm() {
        return jspForm;
    }

    public JTable getJtbForm() {
        return jtbForm;
    }
}
