package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Rectangle;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JInternalFrame;
import javax.swing.border.TitledBorder;

/*
 * @author Acássio
 */
public class AtividadeView extends JInternalFrame {

    private JPanelPadrao jpAtividade;

    public AtividadeView() {
        jpAtividade = new JPanelPadrao();
        setBorder(BorderFactory.createEtchedBorder());
        setTitle("Atividades");
        setFont(new Font("Arial", 0, 12));
        setFrameIcon(new ImageIcon(getClass().getResource("/icons/Bus-64.png")));
        setNormalBounds(new Rectangle(0, 0, 0, 0));

        jpAtividade.getJpTabela().setBorder(BorderFactory.createCompoundBorder(BorderFactory.createEtchedBorder(),
                BorderFactory.createTitledBorder(BorderFactory.createEmptyBorder(1, 1, 1, 1),
                        "Atividades", TitledBorder.LEFT, TitledBorder.BELOW_TOP, new Font("Arial", 1, 12),
                        Color.gray)));
        
        jpAtividade.getJbBuscar().setEnabled(false);
        jpAtividade.getJbExcluir().setEnabled(false);
        jpAtividade.getJbNovo().setEnabled(false);
        
        //**Os botões abaixo passarão a funcionar
        //jpAtividade.getJbSalvar().setEnabled(false);
        //jpAtividade.getJbImportar().setEnabled(false);
        
        setVisible(true);
        getContentPane().setLayout(new BoxLayout(getContentPane(), BoxLayout.LINE_AXIS));
        getContentPane().add(jpAtividade, BorderLayout.LINE_START);
        pack();
    }

    public JPanelPadrao getJpAtividade() {
        return jpAtividade;
    }
}
