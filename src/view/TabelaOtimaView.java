/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Rectangle;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JInternalFrame;
import javax.swing.border.TitledBorder;

/**
 *
 * @author Acássio
 */
public class TabelaOtimaView extends JInternalFrame {

    private JPanelPadrao jpTabOtima;

    public TabelaOtimaView() {
        jpTabOtima = new JPanelPadrao();
        setBorder(BorderFactory.createEtchedBorder());
        setTitle("Tabela de Horários");
        setFont(new Font("Arial", 0, 12));
        setFrameIcon(new ImageIcon(getClass().getResource("/icons/Bus-64.png")));
        setNormalBounds(new Rectangle(0, 0, 0, 0));

        jpTabOtima.getJpTabela().setBorder(BorderFactory.createCompoundBorder(BorderFactory.createEtchedBorder(),
                BorderFactory.createTitledBorder(BorderFactory.createEmptyBorder(1, 1, 1, 1),
                        "Tabela de Horários", TitledBorder.LEFT, TitledBorder.BELOW_TOP, new Font("Arial", 1, 12),
                        Color.gray)));

        jpTabOtima.getJbBuscar().setEnabled(false);
        jpTabOtima.getJbExcluir().setEnabled(false);
        jpTabOtima.getJbNovo().setEnabled(false);
        jpTabOtima.getJbImportar().setEnabled(false);
        
        //**Os botões abaixo passarão a funcionar
        //jpTabOtima.getJbSalvar().setEnabled(false);


        setVisible(true);
        getContentPane().setLayout(new BoxLayout(getContentPane(), BoxLayout.LINE_AXIS));
        getContentPane().add(jpTabOtima, BorderLayout.LINE_START);
        pack();
    }

    public JPanelPadrao getJpTabOtima() {
        return jpTabOtima;
    }
}
