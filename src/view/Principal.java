/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Toolkit;
import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTextPane;
import javax.swing.JTree;
import javax.swing.LayoutStyle;
import javax.swing.WindowConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.MatteBorder;
import javax.swing.border.SoftBevelBorder;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;

/**
 *
 * @author Acássio
 */
public class Principal extends JFrame {

    private JButton jbVeriProblema;
    private JCheckBoxMenuItem jcbmiGlpk;
    private JCheckBoxMenuItem jcbmiLpSove;
    private JDesktopPane jdpPrin;
    private JMenu jmConfigPrin;
    private JMenu jmConfigSolverPrin;
    private JMenu jmPrinArq;
    private JMenu jmSobre;
    private JMenu jmUsuarioPrin;
    private JMenuBar jmbPrin;
    private JMenuItem jmiAbrirPrin;
    private JMenuItem jmiAlterarSenha;
    private JMenuItem jmiNovoUsuario;
    private JMenuItem jmiSairPrin;
    private JMenuItem jmiSalvarComoPrin;
    private JMenuItem jmiSalvarPrin;
    private JMenuItem jmiTempoLimite;
    private JPanel jpBotao;
    private JPanel jpEsquerdoPrin;
    private JPanel jpInferiorPrin;
    private JPanel jpSuperiorPrin;
    private JSeparator jsepEsquerdoPrin;
    private JSeparator jsepInferiorPrin;
    private JScrollPane jspArvore;
    private JScrollPane jspPainelTexto;
    private JTextPane jtpLogPrin;
    private JTree jtrPrin;

    public Principal() {
        jpEsquerdoPrin = new JPanel();
        jpBotao = new JPanel();
        jbVeriProblema = new JButton();
        jspArvore = new JScrollPane();
        jtrPrin = new JTree();
        jsepEsquerdoPrin = new JSeparator();
        jspPainelTexto = new JScrollPane();
        jtpLogPrin = new JTextPane();
        jpSuperiorPrin = new JPanel();
        jpInferiorPrin = new JPanel();
        jsepInferiorPrin = new JSeparator();
        jdpPrin = new JDesktopPane();
        jmbPrin = new JMenuBar();
        jmPrinArq = new JMenu();
        jmiAbrirPrin = new JMenuItem();
        jmiSalvarPrin = new JMenuItem();
        jmiSalvarComoPrin = new JMenuItem();
        jmiSairPrin = new JMenuItem();
        jmConfigPrin = new JMenu();
        jmConfigSolverPrin = new JMenu();
        jcbmiLpSove = new JCheckBoxMenuItem();
        jcbmiGlpk = new JCheckBoxMenuItem();
        jmiTempoLimite = new JMenuItem();
        jmUsuarioPrin = new JMenu();
        jmiAlterarSenha = new JMenuItem();
        jmiNovoUsuario = new JMenuItem();
        jmSobre = new JMenu();

        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        getContentPane().setLayout(new BorderLayout(2, 3));

        jpEsquerdoPrin.setBorder(new MatteBorder(null));

        jpBotao.setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED));
        jpBotao.setLayout(new GridBagLayout());

        jbVeriProblema.setFont(new Font("Arial", 1, 12)); // NOI18N
        jbVeriProblema.setText("Resolver");
        jbVeriProblema.setBorder(BorderFactory.createCompoundBorder(new SoftBevelBorder(BevelBorder.RAISED),
                BorderFactory.createBevelBorder(BevelBorder.RAISED)));

        jpBotao.add(jbVeriProblema, new GridBagConstraints());
        
        DefaultMutableTreeNode arvoreNoCadastros = new DefaultMutableTreeNode("Problema");
        arvoreNoCadastros.add(new DefaultMutableTreeNode("Veículos"));
        arvoreNoCadastros.add(new DefaultMutableTreeNode("Linhas e Horários"));
        arvoreNoCadastros.add(new DefaultMutableTreeNode("Resultados"));
        jtrPrin.setModel(new DefaultTreeModel(arvoreNoCadastros));
        jtrPrin.setCursor(new Cursor(Cursor.HAND_CURSOR));

        jspArvore.setViewportView(jtrPrin);
        jspPainelTexto.setViewportView(jtpLogPrin);

        GroupLayout jpEsquerdoPrinLayout = new GroupLayout(jpEsquerdoPrin);
        jpEsquerdoPrin.setLayout(jpEsquerdoPrinLayout);

        jpEsquerdoPrinLayout.setHorizontalGroup(
                jpEsquerdoPrinLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                .addComponent(jpBotao, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jspArvore)
                .addGroup(jpEsquerdoPrinLayout.createSequentialGroup()
                        .addGroup(jpEsquerdoPrinLayout.createParallelGroup(GroupLayout.Alignment.TRAILING, false)
                                .addComponent(jspPainelTexto, GroupLayout.DEFAULT_SIZE, 323, Short.MAX_VALUE)
                                .addComponent(jsepEsquerdoPrin))
                        .addGap(0, 0, Short.MAX_VALUE))
        );

        jpEsquerdoPrinLayout.setVerticalGroup(
                jpEsquerdoPrinLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                .addGroup(jpEsquerdoPrinLayout.createSequentialGroup()
                        .addComponent(jpBotao, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
                                GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jspArvore, GroupLayout.PREFERRED_SIZE, 215,
                                GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jsepEsquerdoPrin, GroupLayout.PREFERRED_SIZE,
                                GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jspPainelTexto, GroupLayout.DEFAULT_SIZE, 278, Short.MAX_VALUE))
        );

        getContentPane().add(jpEsquerdoPrin, BorderLayout.LINE_START);

        jpSuperiorPrin.setBackground(new Color(255, 255, 255));
        jpSuperiorPrin.setBorder(BorderFactory.createLineBorder(new Color(0, 51, 204)));

        GroupLayout jpSuperiorPrinLayout = new GroupLayout(jpSuperiorPrin);
        jpSuperiorPrin.setLayout(jpSuperiorPrinLayout);
        jpSuperiorPrinLayout.setHorizontalGroup(
                jpSuperiorPrinLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                .addGap(0, 987, Short.MAX_VALUE)
        );
        jpSuperiorPrinLayout.setVerticalGroup(
                jpSuperiorPrinLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                .addGap(0, 46, Short.MAX_VALUE)
        );

        getContentPane().add(jpSuperiorPrin, BorderLayout.PAGE_START);

        GroupLayout jpInferiorPrinLayout = new GroupLayout(jpInferiorPrin);
        jpInferiorPrin.setLayout(jpInferiorPrinLayout);
        jpInferiorPrinLayout.setHorizontalGroup(
                jpInferiorPrinLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                .addComponent(jsepInferiorPrin, GroupLayout.DEFAULT_SIZE, 989, Short.MAX_VALUE)
        );

        jpInferiorPrinLayout.setVerticalGroup(
                jpInferiorPrinLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jpInferiorPrinLayout.createSequentialGroup()
                        .addGap(0, 0, 0)
                        .addComponent(jsepInferiorPrin, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE))
        );

        getContentPane().add(jpInferiorPrin, BorderLayout.PAGE_END);

        jdpPrin.setBackground(new Color(204, 204, 204));
        jdpPrin.setBorder(new SoftBevelBorder(BevelBorder.RAISED));

        GroupLayout jdpPrinLayout = new GroupLayout(jdpPrin);
        jdpPrin.setLayout(jdpPrinLayout);
        jdpPrinLayout.setHorizontalGroup(
                jdpPrinLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                .addGap(0, 656, Short.MAX_VALUE)
        );
        jdpPrinLayout.setVerticalGroup(
                jdpPrinLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                .addGap(0, 538, Short.MAX_VALUE)
        );

        getContentPane().add(jdpPrin, BorderLayout.CENTER);

        jmPrinArq.setText("Arquivo");

        jmiAbrirPrin.setText("Abrir");
        jmPrinArq.add(jmiAbrirPrin);

        jmiSalvarPrin.setText("Salvar");
        jmPrinArq.add(jmiSalvarPrin);

        jmiSalvarComoPrin.setText("Salvar Como");
        jmPrinArq.add(jmiSalvarComoPrin);

        jmiSairPrin.setText("Sair");
        jmPrinArq.add(jmiSairPrin);

        jmbPrin.add(jmPrinArq);
        
        //***Algumas opções ficarão indisponíveis até que os módulos seja desenvolvidos***
        
        
        //jmConfigPrin.setText("Configurações");

        //jmConfigSolverPrin.setText("Solver");

        //jcbmiLpSove.setSelected(true);
        //jcbmiLpSove.setText("Lp Solve");
        //jmConfigSolverPrin.add(jcbmiLpSove);

        //jcbmiGlpk.setSelected(true);
        //jcbmiGlpk.setText("GLPK");
        //jmConfigSolverPrin.add(jcbmiGlpk);

        //jmConfigPrin.add(jmConfigSolverPrin);

        //jmiTempoLimite.setText("Tempo Limite");
        //jmConfigPrin.add(jmiTempoLimite);

        //jmbPrin.add(jmConfigPrin);

        //jmUsuarioPrin.setText("Usuário");

        //jmiAlterarSenha.setText("Alterar Senha");
        //jmUsuarioPrin.add(jmiAlterarSenha);

        //jmiNovoUsuario.setText("Novo Usuário");
        //jmUsuarioPrin.add(jmiNovoUsuario);

        //jmbPrin.add(jmUsuarioPrin);

        //jmSobre.setText("Sobre");
        //jmbPrin.add(jmSobre);

        setJMenuBar(jmbPrin);
        
        jmbPrin.setVisible(true);
        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/icons/Bus-64.png")));
        setTitle("OptDiesel");
        pack();
        setExtendedState(JFrame.MAXIMIZED_BOTH);
    }

    public JButton getJbVeriProblema() {
        return jbVeriProblema;
    }

    public JCheckBoxMenuItem getJcbmiGlpk() {
        return jcbmiGlpk;
    }

    public JCheckBoxMenuItem getJcbmiLpSove() {
        return jcbmiLpSove;
    }

    public JDesktopPane getJdpPrin() {
        return jdpPrin;
    }

    public JMenu getJmConfigPrin() {
        return jmConfigPrin;
    }

    public JMenu getJmConfigSolverPrin() {
        return jmConfigSolverPrin;
    }

    public JMenu getJmPrinArq() {
        return jmPrinArq;
    }

    public JMenu getJmSobre() {
        return jmSobre;
    }

    public JMenu getJmUsuarioPrin() {
        return jmUsuarioPrin;
    }

    public JMenuBar getJmbPrin() {
        return jmbPrin;
    }

    public JMenuItem getJmiAbrirPrin() {
        return jmiAbrirPrin;
    }

    public JMenuItem getJmiAlterarSenha() {
        return jmiAlterarSenha;
    }

    public JMenuItem getJmiNovoUsuario() {
        return jmiNovoUsuario;
    }

    public JMenuItem getJmiSairPrin() {
        return jmiSairPrin;
    }

    public JMenuItem getJmiSalvarComoPrin() {
        return jmiSalvarComoPrin;
    }

    public JMenuItem getJmiSalvarPrin() {
        return jmiSalvarPrin;
    }

    public JMenuItem getJmiTempoLimite() {
        return jmiTempoLimite;
    }

    public JPanel getJpBotao() {
        return jpBotao;
    }

    public JPanel getJpEsquerdoPrin() {
        return jpEsquerdoPrin;
    }

    public JPanel getJpInferiorPrin() {
        return jpInferiorPrin;
    }

    public JPanel getJpSuperiorPrin() {
        return jpSuperiorPrin;
    }

    public JSeparator getJsepEsquerdoPrin() {
        return jsepEsquerdoPrin;
    }

    public JSeparator getJsepInferiorPrin() {
        return jsepInferiorPrin;
    }

    public JScrollPane getJspArvore() {
        return jspArvore;
    }

    public JScrollPane getJspPainelTexto() {
        return jspPainelTexto;
    }

    public JTextPane getJtpLogPrin() {
        return jtpLogPrin;
    }

    public JTree getJtrPrin() {
        return jtrPrin;
    }
}
