package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Rectangle;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JInternalFrame;
import javax.swing.border.TitledBorder;

/*
 * @author Acássio
 */
public class VeiculoView extends JInternalFrame {

    private JPanelPadrao jpVeiculo;

    public VeiculoView() {
        jpVeiculo = new JPanelPadrao();
        setBorder(BorderFactory.createEtchedBorder());
        setTitle("Veículos");
        setFont(new Font("Arial", 0, 12));
        setFrameIcon(new ImageIcon(getClass().getResource("/icons/Bus-64.png")));
        setNormalBounds(new Rectangle(0, 0, 0, 0));

        jpVeiculo.getJpTabela().setBorder(BorderFactory.createCompoundBorder(BorderFactory.createEtchedBorder(),
                BorderFactory.createTitledBorder(BorderFactory.createEmptyBorder(1, 1, 1, 1),
                        "Veículos", TitledBorder.LEFT, TitledBorder.BELOW_TOP, new Font("Arial", 1, 12),
                        Color.gray)));
        
        jpVeiculo.getJbBuscar().setEnabled(false);
        jpVeiculo.getJbExcluir().setEnabled(false);
        jpVeiculo.getJbNovo().setEnabled(false);
        
        //**Os botões abaixo passarão a funcionar
        //jpVeiculo.getJbSalvar().setEnabled(false);
        //jpVeiculo.getJbImportar().setEnabled(false);
        
        setVisible(true);
        getContentPane().setLayout(new BoxLayout(getContentPane(), BoxLayout.LINE_AXIS));
        getContentPane().add(jpVeiculo, BorderLayout.LINE_START);
        pack();
    }

    public JPanelPadrao getJpVeiculo() {
        return jpVeiculo;
    }
}
